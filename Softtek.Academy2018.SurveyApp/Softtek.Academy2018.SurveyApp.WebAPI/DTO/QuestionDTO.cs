﻿namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
    public class QuestionDTO
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public bool IsActive { get; set; }

        public int QuestionTypeId { get; set; }
    }
}