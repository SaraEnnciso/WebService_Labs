﻿using Softtek.Academy2018.SurveyApp.Business.Implementations;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Implementations;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
   [RoutePrefix("Questions")]
   public class QuestionController : ApiController
   {
       private readonly QuestionService _questionService;

       public QuestionController()
       {
           IQuestionRepository repository = new QuestionDataRepository();
           this._questionService = new QuestionService(repository);
       }

       [Route("{id}")]
       [HttpGet]
       public IHttpActionResult Get([FromUri]  int id)
       {
           Question question = _questionService.Get(id);

           if (question == null) return NotFound();

            QuestionDTO questionDTO = new QuestionDTO
            {
                Id = question.Id,
                Text = question.Text,
                QuestionTypeId = question.QuestionTypeId,
                IsActive = question.IsActive
            };

           return Ok(questionDTO);
       }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] QuestionDTO questionDTO)
        {
            if (questionDTO == null) return BadRequest("Request is null");

            Question question = new Question
            {
                Text = questionDTO.Text,
                QuestionTypeId = questionDTO.QuestionTypeId
            };

            int id = _questionService.Add(question);

            if (id <= 0) return BadRequest("Unable to create question");

            var payload = new { questionId = id };

            return Ok(payload);
        }

        [Route("Update")]
        [HttpPost]
        public IHttpActionResult Update([FromBody] QuestionDTO questionDTO)
        {
            if (questionDTO == null) return BadRequest("Request is null");

            Question question = new Question
            {
                Id = questionDTO.Id,
                Text = questionDTO.Text,
                QuestionTypeId = questionDTO.QuestionTypeId,
            };

            bool updated = _questionService.Update(question);

            if (!updated) return BadRequest("Unable to update question");

            var payload = new { questionId = question.Id };

            return Ok("Project " + payload + " updated succesfully");
        }

    }

}
