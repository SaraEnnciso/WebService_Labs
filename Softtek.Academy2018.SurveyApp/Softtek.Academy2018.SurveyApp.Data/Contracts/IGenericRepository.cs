﻿namespace Softtek.Academy2018.SurveyApp.Data.Contracts
{
    public interface IGenericRepository<T> where T : class
    {
        int Add(T clss);

        T Get(int id);

        bool Update(T clss);

        bool Delete(T clss);
    }

}
