﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class QuestionDataRepository : IQuestionRepository
    {
        public int Add(Question clss)
        {
            clss.CreatedDate = DateTime.Now;

            if(clss.QuestionTypeId == 1)
            {
                clss.IsActive = true;
            }

            using (var db = new SurveyDbContext())
            {
                db.Questions.Add(clss);
                db.SaveChanges();
                return clss.Id;
            }
        }

        public bool Delete(Question clss)
        {
            using (var db = new SurveyDbContext())
            {
                db.Questions.Attach(clss);
                db.Entry(clss).Property(c => c.IsActive).CurrentValue = false;
                db.SaveChanges();
                return true;
            }
        }

        public Question Get(int id)
        {
            using (var db = new SurveyDbContext())
            {
                return db.Questions.Include("Surveys").AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public bool Update(Question clss)
        {
            using (var db = new SurveyDbContext())
            {
                Question question = db.Questions.SingleOrDefault(x => x.Id == clss.Id);

                if (question == null) return false;

                db.Questions.Attach(clss);
                db.Entry(clss).Property(q => q.Text).IsModified = true;
                db.Entry(clss).Property(q => q.IsActive).IsModified = true;
                db.Entry(clss).Property(q => q.QuestionTypeId).IsModified = true;
                db.Entry(clss).Property(q => q.ModifiedDate).CurrentValue = DateTime.Now;
                db.SaveChanges();

                return true;
            }
        }

        public List<Question> GetAll()
        {
            using (var db = new SurveyDbContext())
            {
                return db.Questions.ToList();
            }
        }
        
    }
}
