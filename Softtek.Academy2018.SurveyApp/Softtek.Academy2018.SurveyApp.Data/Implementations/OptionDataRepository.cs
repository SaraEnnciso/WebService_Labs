﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class OptionDataRepository : IOptionRepository
    {
        public int Add(Option option)
        {
            using (var ctx = new SurveyDbContext())
            {
                ctx.Options.Add(option);
                ctx.SaveChanges();

                return option.Id;
            }            
        }

        public bool Delete(Option option)
        {
            throw new NotImplementedException();
        }

        public bool Exist(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.Options.Any(op => op.Id == id);
            }
        }

        public bool ExistInQuestion(Option option)
        {
            throw new NotImplementedException();
        }

        public Option Get(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<Option> GetAll()
        {
            throw new NotImplementedException();
        }

        public bool Update(Option option)
        {
            throw new NotImplementedException();
        }
    }
}
