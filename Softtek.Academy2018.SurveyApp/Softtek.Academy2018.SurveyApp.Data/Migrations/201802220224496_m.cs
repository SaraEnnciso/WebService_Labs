namespace Softtek.Academy2018.SurveyApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.QuestionTypes", "Description", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Surveys", "Description", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Surveys", "Description", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.QuestionTypes", "Description", c => c.String(nullable: false, maxLength: 200));
        }
    }
}
