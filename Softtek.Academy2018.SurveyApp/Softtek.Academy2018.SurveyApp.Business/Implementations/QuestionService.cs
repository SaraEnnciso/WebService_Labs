﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.Data.Contracts;

namespace Softtek.Academy2018.SurveyApp.Business.Implementations
{
    public class QuestionService : IQuestionService
    {
        private readonly IQuestionRepository repository;

        public QuestionService(IQuestionRepository repository)
        {
            this.repository = repository;
        }

        public int Add(Question clss)
        {
            if (string.IsNullOrEmpty(clss.Text)) return 0;

            if (clss.QuestionTypeId <= 0 || clss.QuestionTypeId > 3) return 0;

            if (clss.Text.Length > 200) return 0;

            int id = repository.Add(clss);

            return id;
        }

        public bool Update(Question clss)
        {
            if (clss.Id <= 0) return false;

            Question question = repository.Get(clss.Id);

            if (question != null && !question.IsActive) return false;

            if (question.Surveys != null && question.Surveys.Count > 0) return false;

            return repository.Update(clss);
        }

        public bool Delete(Question clss)
        {
            if (clss.Id <= 0) return false;

            Question question = repository.Get(clss.Id);

            if (question == null || !question.IsActive) return false;

            return repository.Delete(clss);
        }

        public Question Get(int id)
        {
            if (id <= 0) return null;

            Question question = repository.Get(id);

            if (question != null && !question.IsActive) return null;

            return question;
        }

        public List<Question> GetAll()
        {
            return repository.GetAll();
        }
    }
}
