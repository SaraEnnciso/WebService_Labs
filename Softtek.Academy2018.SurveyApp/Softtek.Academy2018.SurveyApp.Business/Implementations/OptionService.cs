﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Softtek.Academy2018.SurveyApp.Business.Implementation
{
    public class OptionService : IOptionService
    {
        private readonly IOptionRepository _optionRepository;

        public OptionService(IOptionRepository optionRepository)
        {
            _optionRepository = optionRepository;
        }

        public int Add(Option option)
        {
            if (string.IsNullOrEmpty(option.Text) || string.IsNullOrWhiteSpace(option.Text)) return 0;

            if (_optionRepository.Exist(option.Id)) return 0;

            int id = _optionRepository.Add(option);
            return id;            
        }

        public ICollection<Option> GetAll()
        {            
            return _optionRepository.GetAll();            
        }

        public bool Update(Option option)
        {
            if (_optionRepository.ExistInQuestion(option)) return false;

            return _optionRepository.Update(option);
        }
    }
}
